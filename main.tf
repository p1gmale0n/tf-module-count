variable enable {
  default = false
}

module "random" {
  count = var.enable ? 1 : 0
  source = "./module"
}
