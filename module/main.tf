resource "random_string" "temp" {
  length  = 32
  special = false
  upper   = true
}
